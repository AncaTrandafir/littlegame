package com.company;

import java.awt.*;
import java.awt.image.BufferedImage;

public class SmartEnemy extends GameObject {            // SmartEnemy urmareste playerul

    private Handler handler;
    private Trail trail;
    private GameObject player;
    GraphSheet graphSheet;
    BufferedImage smarteEnemyImage;

    public SmartEnemy(int x, int y, ID id, Handler handler) {
        super(x, y, id);
        this.handler = handler;

        graphSheet = new GraphSheet(Game.bufferedImage);   // instantiem un GraphSheet cu un BufferedImaged incarcat in Game cu loader();
        smarteEnemyImage = graphSheet.grabImage(4, 5, 16, 16);

        for (int i=0; i < handler.objectList.size(); i++ )
            if (handler.objectList.get(i).getId() == ID.Player)
                player = handler.objectList.get(i);

    }

    @Override
    public void tick() {
        x += velX;
        y += velY;


        // !!!!! NOTA: Pt ca diffX si diffY sunt float, treb si x si y lui object sa fie float;
        // Dar putem desena dreptunghi doar cu x si y intregi; deci ele vor fi float si vom facem cast la int in cls GameObject si derivatele lui


        float diffX = x - player.getX() - 8;
        float diffY = y - player.getY() - 8;
        float distance = (float) Math.sqrt( (x-player.getX()) * (x-player.getX()) + (y-player.getY()) * (y-player.getY()) );

        velX = (float) ((-1.3 / distance) * diffX);
        velY = (float) ((-1.3 / distance) * diffY);

        // Daca este negativ(urca), negativ * negativ = pozitiv si deci se intoarce, coboara pe aceeasi traiectorie, daca sa zicem x = 0
        // Game.HEIGHT sa nu iasa din perimetrul ferestrei pe inaltime; Daca depaseste inaltimea ferestrei, se intoarce
        if (y <= 0 || y >= Game.HEIGHT - 32) velY *= -1;
        if (x <= 0 || x >= Game.WIDTH - 16) velX *= -1;

        // La enemy, adaugam obiect tip trail
    //    handler.addObject(new Trail((int)x, (int)y, ID.Trail, new Color(130, 38, 39), 14, 14, 0.04f, handler));
    }

    @Override
    public void render(Graphics g) {
//        g.setColor(new Color(130, 38, 39));
//        g.fillRect((int)x, (int)y, 18, 18 );
        g.drawImage(smarteEnemyImage, (int)x, (int)y, null);
    }

    @Override
    public Rectangle getBounds() {              // returneaza un dreptunghi de dimensiunile astea
        return new Rectangle((int)x, (int)y, 18, 18);
    }



}
