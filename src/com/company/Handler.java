package com.company;

import java.awt.*;
import java.util.LinkedList;

public class Handler {      // loop through all of the objects, update them and render them on the screen


    LinkedList<GameObject> objectList = new LinkedList<GameObject>();

    public int speed = 5; // pt a creste viteza cand cumpara viteza; o accesez din cls KeyInput, pt ca acolo am velocitatile setate pt Player.



    public void tick(){     // updates all game objects
        for (int i=0; i<objectList.size(); i++) {   // loop through all objects
            GameObject tempGameObject = objectList.get(i);
            tempGameObject.tick();
        }
    }

    public void render(Graphics g){       // renders all game objects
        for (int i=0; i<objectList.size(); i++) {   // loop through all objects
            GameObject tempGameObject = objectList.get(i);
            tempGameObject.render(g);
        }
    }

    public void addObject(GameObject object) {
        objectList.add(object);
    }

    public void removeObject(GameObject object) {
        objectList.remove(object);
    }

    public void clearEnemies() {
        for (int i=0; i<objectList.size(); i++)  // golim lista de inamici + obiecte tip MenuAnimation + trail
            if (objectList.get(i).getId() == ID.BasicEnemy || this.objectList.get(i).getId() == ID.SmartEnemy || this.objectList.get(i).getId() == ID.FastEnemy || this.objectList.get(i).getId() == ID.Trail || this.objectList.get(i).getId() == ID.BossEnemy || this.objectList.get(i).getId() == ID.BulletBossEnemy)
                 removeObject(objectList.get(i));
    }

    public void clearMenuAnimation() {
        for (int i=0; i<objectList.size(); i++)  // golim lista de obiecte tip MenuAnimation + trail
            if (this.objectList.get(i).getId() == ID.Trail || this.objectList.get(i).getId() == ID.MenuAnimation)
                removeObject(objectList.get(i));
    }

    public void clearPlayer() {  // folosim in starea de End
        for (int i=0; i<objectList.size(); i++)  // golim lista de obiecte tip MenuAnimation + trail
            if (this.objectList.get(i).getId() == ID.Player)
                removeObject(objectList.get(i));
    }
}
