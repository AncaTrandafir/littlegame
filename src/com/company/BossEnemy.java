package com.company;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Random;

public class BossEnemy extends GameObject {

    private Handler handler;
  //  private Trail trail;
    private int timer;
    private int timer2;
    private Random random;

    GraphSheet graphSheet;
    BufferedImage bossEnemyImage;

    public BossEnemy(int x, int y, ID id, Handler handler) {
        super(x, y, id);
        this.handler = handler;

        velX = 0;
        velY = 4;

        timer = 33;
        timer2 = 50;

        graphSheet = new GraphSheet(Game.bufferedImage);   // instantiem un GraphSheet cu un BufferedImaged incarcat in Game cu loader();
        bossEnemyImage = graphSheet.grabImage(1, 3, 96, 96);        // sunt inversate, de verificat

    }

    @Override
    public void tick() {
        x += velX;
        y += velY;

        if (timer <= 0) velY = 0;  // coboara si se opreste
            else timer--;   // descreste pana se opreste: velY = 0;

        if (timer <= 0) timer2--; // Dupa ce s-a oprit din coborare, sta putin, cat decrementeaza timer2

        if (timer2 <= 0) {               // cand timer2 a expirat, incepe miscare stanga-dreapta
            if (velX == 0) velX = 2;        // setam viteza pe x
            if (x <= 0 || x >= Game.WIDTH - 108) velX *= -1;         // cu bounce inapoi cand atinge peretii

            // ii cresc progresiv viteza pe directia stanga dreapta
            if (velX > 0) velX += 0.005f;
                else if (velX <= 0) velX -= -0.005f;
             velX = Game.clamp(velX, -5, 5);   // ii pun niste limite, sa nu creasca la infinit viteza

            random = new Random();

            int spawn = random.nextInt(30); // returns a value between 0 and 10
           // Cu cat limita pt random e mai mare cu atat scade probabilitate sa aleaga 0
            // Iar cand alege 0, creeaza un obiect nou;
            // Asa stabilim frecventa gloantelor, care este random
            if (spawn == 0) handler.addObject(new BulletBossEnemy((int)x, (int)y, ID.BulletBossEnemy, handler));



        }



     //   handler.addObject(new Trail((int)x, (int)y, ID.Trail, new Color(235, 63, 29) , 96, 96, 0.008f, handler));  // 0.1 pt viata
    }

    @Override
    public void render(Graphics g) {
//        g.setColor(new Color(235, 63, 29));
//        g.fillRect((int)x, (int)y, 96, 96 );      // ultimele 2 valori dau dimensiunea: width si height
            g.drawImage(bossEnemyImage, (int)x, (int)y, null);
    }

    @Override
    public Rectangle getBounds() {              // returneaza un dreptunghi de dimensiunile astea
        return new Rectangle((int)x, (int)y, 96, 96);
    }



}
