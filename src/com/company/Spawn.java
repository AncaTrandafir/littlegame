package com.company;

import java.util.Random;

import static com.company.Game.HEIGHT;
import static com.company.Game.WIDTH;

public class Spawn {            // aici construiesc nivelele

    private Handler handler;
    private HUD hud;

    private Random random;

    private int scoreKeep;

    public Spawn(Handler handler, HUD hud) {
        this.handler = handler;
        this.hud = hud;
    }

    // NEXT LEVEL: tranzitia la nivel superior -> pe masura ce scorul atinge un anumit prag, avansam la nivel urm


    public void tick() {
        scoreKeep = hud.getScore();
        random = new Random();
        if (scoreKeep % 500 == 0) { // pe msaura ce atingem multiplu de 500 pct la scor, trecem la nivel urm
            hud.setLevel(hud.getLevel() + 1);

            // cu fiecare nivel crestem intensitatea jocului, gen adaugam inamici
            if (hud.getLevel() == 2)
                handler.addObject(new BasicEnemy(random.nextInt(WIDTH), random.nextInt(HEIGHT), ID.BasicEnemy, handler));

            else if (hud.getLevel() == 3)
                for (int i = 0; i < 3; i++)         // inca 3 inamici pt nivel 3
                    handler.addObject(new BasicEnemy(random.nextInt(WIDTH), random.nextInt(HEIGHT), ID.BasicEnemy, handler));

//            else if (hud.getLevel() == 4)
//                handler.addObject(new FastEnemy(random.nextInt(WIDTH), random.nextInt(HEIGHT), ID.FastEnemy, handler));

            else if (hud.getLevel() == 5) {
                for (int i=0; i<handler.objectList.size(); i++)
                    handler.clearEnemies(); // golim inamicii
                handler.addObject(new BossEnemy((WIDTH / 2 - 48), -120, ID.BossEnemy, handler));
            }

            else if (hud.getLevel() == 7) {
                for (int i=0; i<handler.objectList.size(); i++)
                    handler.clearEnemies(); // golim inamicii
                handler.addObject(new SmartEnemy(random.nextInt(WIDTH), random.nextInt(HEIGHT), ID.SmartEnemy, handler));
                 for (int i = 0; i < 4; i++)
                        handler.addObject(new BasicEnemy(random.nextInt(WIDTH), random.nextInt(HEIGHT), ID.BasicEnemy, handler));
                 handler.addObject(new FastEnemy(random.nextInt(WIDTH), random.nextInt(HEIGHT), ID.FastEnemy, handler));
            }

            else if (hud.getLevel() == 9) {
//                for (int i = 0; i < 3; i++)
//                    handler.addObject(new BasicEnemy(random.nextInt(WIDTH), random.nextInt(HEIGHT), ID.BasicEnemy, handler));
                handler.addObject(new FastEnemy(random.nextInt(WIDTH), random.nextInt(HEIGHT), ID.FastEnemy, handler));
                handler.addObject(new SmartEnemy(random.nextInt(WIDTH), random.nextInt(HEIGHT), ID.SmartEnemy, handler));
            }

            else if (hud.getLevel() == 10) {
                for (int i=0; i<handler.objectList.size(); i++)
                    handler.clearEnemies(); // golim inamicii
                handler.addObject(new BossEnemy((WIDTH / 2 - 48), -120, ID.BossEnemy, handler));
                handler.addObject(new BasicEnemy(random.nextInt(WIDTH), random.nextInt(HEIGHT), ID.BasicEnemy, handler));
                handler.addObject(new FastEnemy(random.nextInt(WIDTH), random.nextInt(HEIGHT), ID.FastEnemy, handler));
                handler.addObject(new SmartEnemy(random.nextInt(WIDTH), random.nextInt(HEIGHT), ID.SmartEnemy, handler));
            }

            else if (hud.getLevel() == 13) {
                for (int i=0; i<handler.objectList.size(); i++)
                    handler.clearEnemies(); // golim inamicii
                for (int i = 0; i < 4; i++) {
                    handler.addObject(new BasicEnemy(random.nextInt(WIDTH), random.nextInt(HEIGHT), ID.BasicEnemy, handler));
                    handler.addObject(new FastEnemy(random.nextInt(WIDTH), random.nextInt(HEIGHT), ID.FastEnemy, handler));
                }
                handler.addObject(new SmartEnemy(random.nextInt(WIDTH), random.nextInt(HEIGHT), ID.SmartEnemy, handler));
            }

            else if (hud.getLevel() == 16) {
                handler.addObject(new BasicEnemy(random.nextInt(WIDTH), random.nextInt(HEIGHT), ID.BasicEnemy, handler));
                handler.addObject(new SmartEnemy(random.nextInt(WIDTH), random.nextInt(HEIGHT), ID.SmartEnemy, handler));
                handler.addObject(new FastEnemy(random.nextInt(WIDTH), random.nextInt(HEIGHT), ID.FastEnemy, handler));
            }

            else if (hud.getLevel() == 20) {
                for (int i=0; i<handler.objectList.size(); i++)
                    handler.clearEnemies(); // golim inamicii
                handler.addObject(new BossEnemy((WIDTH / 2 - 48), -120, ID.BossEnemy, handler));
                for (int i = 0; i < 5; i++) {
                    handler.addObject(new BasicEnemy(random.nextInt(WIDTH), random.nextInt(HEIGHT), ID.BasicEnemy, handler));
                    handler.addObject(new SmartEnemy(random.nextInt(WIDTH), random.nextInt(HEIGHT), ID.SmartEnemy, handler));
                    handler.addObject(new FastEnemy(random.nextInt(WIDTH), random.nextInt(HEIGHT), ID.FastEnemy, handler));
                }
            }
        }

    }



}
