package com.company;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Random;

public class BulletBossEnemy extends GameObject {

    private Handler handler;
    private Trail trail;
    private Random random;

    GraphSheet graphSheet;
    BufferedImage bulletImage;

    public BulletBossEnemy(int x, int y, ID id, Handler handler) {
        super(x, y, id);
        this.handler = handler;

        random = new Random();

        velX = (random.nextInt(5 - -5) + -5);   // returneaza un nr random intre -5 si 5
       // velX = 2;
        velY = 5;

        graphSheet = new GraphSheet(Game.bufferedImage);   // instantiem un GraphSheet cu un BufferedImaged incarcat in Game cu loader();
        bulletImage = graphSheet.grabImage(5, 5, 16, 16);

    }

    @Override
    public void tick() {
        x += velX;
        y += velY;

        if (y >= Game.HEIGHT) handler.removeObject(this); // cand atinge fundul (x = height) dispare; il inlaturam din lista de obiecte

    //    handler.addObject(new Trail((int)x, (int)y, ID.Trail, new Color(235, 63, 29), 8, 8, 0.08f, handler));
    }

    @Override
    public void render(Graphics g) {
//        g.setColor(new Color(235, 63, 29));
//        g.fillRect((int)x, (int)y, 8, 8 );
        g.drawImage(bulletImage, (int)x, (int)y, null);
    }

    @Override
    public Rectangle getBounds() {              // returneaza un dreptunghi de dimensiunile astea
        return new Rectangle((int)x, (int)y, 8, 8);
    }



}
