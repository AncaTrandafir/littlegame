package com.company;

import java.awt.*;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.util.Random;

public class Game extends Canvas implements Runnable {

    private static final  long serialVersionUID = 240840600533728354L;

    public static final int WIDTH = 640, HEIGHT = WIDTH / 12* 9;

    private Thread thread;  // single thread in jocul meu pt ca e simplu
    private boolean running = false;

    public static boolean paused = false;       // pt a pune pauza jocului
                                                // daca pause=true, in render drawString("Game paused), daca pause=false, in tick() se updateaza
                                                // am keyInput(SpaceBar)

     private Handler handler;

    private HUD hud;

    private Spawn spawner;

    private Menu menu;

    private Shop shop;

    // Create a random instance
    private Random r;



    public enum STATE {   // cele trei stari ale jocului nostru, declarate in cls Game
        Menu,
        Game,
        Shop,
        Help,
        End;        // cand se termina viata, se term jocul
    }

    public STATE gameState = STATE.Menu; // we can now pass State like a type;      jocul se deschide cu starea de meniu

    public static BufferedImage bufferedImage;     // o apelam direct cu clasa din Player


    public Game() {

        hud = new HUD();
        handler = new Handler();

        BufferedImageLoader loader = new BufferedImageLoader();  // cream o instanta de BuffredImageLoader, iar cu metoda load(), incarcam fisierul de la path
        bufferedImage = loader.loadImage("res/sheet.png");

        menu = new Menu(this, handler, hud);         // mouseListener pt meniu
        this.addMouseListener(menu);

        spawner = new Spawn(handler, hud);
        this.addKeyListener(new KeyInput(handler, this));     // keyListener pt deplasare player din handler

        AudioPlayer.load(); // statica, importam, direct cu clasa;  -> loads the music
        AudioPlayer.getMusic("music").loop();  // loop sa se repete in fundal
        AudioPlayer.playing = true;   // playing ia valoarea true; cand punem pauza in meniu va fi iar false

        new Window(WIDTH, HEIGHT, "Henna Game -> Anca Trandafir, 2020 :)", this);   // this -> referinta la joc


        shop = new Shop(this, handler, hud);           // instantiem shop, dupa ce am incarcat imaginea, pt ca in shop avem imagini
        this.addMouseListener(shop);            //  mouseListener pt shop

        r = new Random();

        if (gameState == STATE.Menu) {
            for (int i=0; i < 10; i++)      // adaugam nr random de obiecte de tip MenuAnimation
                handler.addObject(new MenuAnimation(r.nextInt(WIDTH), r.nextInt(HEIGHT), ID.MenuAnimation, handler));
        }

        // ----------------- AICI incepe jocul efectiv, doar ca am bagat deja totul in cls Menu
     //   if (gameState == STATE.Game) { // daca este starea de Game imi face obiectele in handler, jocul efectiv, cu handler.addObject



    //        for (int i=0; i<100; i++)      // 100 objects with random X and Y
    //            handler.addObject(new Player(r.nextInt(WIDTH), r.nextInt(HEIGHT), ID.Player));

    //        handler.addObject(new Player(WIDTH / 2 - 32, HEIGHT / 2 - 32, ID.Player, handler));      // pozitionat in centru -> /2
    //        handler.addObject(new Player(WIDTH/2+64, HEIGHT/2+64, ID.Player2));       // Player2, daca as avea

      //      handler.addObject(new BasicEnemy(r.nextInt(WIDTH), r.nextInt(HEIGHT), ID.BasicEnemy, handler));
     //   }





    }



    public synchronized void start(){
        thread = new Thread(this);
        thread.start();
        running = true;     // threadul merge
    }

    public synchronized void stop(){
       try{
           thread.join();       //  allows one thread to wait until another thread completes its execution -> dar noi avem unul singur..?
           running = false;     // threadul nu merge
       }catch(Exception e){
           e.printStackTrace();
       }

    }


    private void tick() {

        if (gameState == STATE.Game) {
            if (!paused) {           // if it's not pause, run everything
                hud.tick();
                spawner.tick();
                handler.tick();

                if (HUD.HEALTH <= 0)        // daca Health scade sub 0, intra in starea de End; Render pt End se face mai jos si mai este cod in cosntructor Game() pt End
                {
                    gameState = Game.STATE.End;  // pt a face render de graphics din cls Menu, stare End
                    for (int i = 0; i < handler.objectList.size(); i++) {   // golim ecranul de inamici si player
                        handler.clearEnemies();
                        handler.clearPlayer();
                    }
                    for (int i = 0; i < 20; i++) {
                        handler.addObject(new MenuAnimation(r.nextInt(WIDTH), r.nextInt(HEIGHT), ID.MenuAnimation, handler));  // adaugam animatii
                    }
                 }

            }

        } else if (gameState == STATE.Menu || gameState == STATE.End) {
            menu.tick();
            handler.tick();

        }
    }


    private void render(){          // Canvas will be updating many times a second so I need a buffer strategy.
        // Pentru cursivitate intre frame-uri. Ele sunt gata si pregatite sa fie incarcate
        BufferStrategy bs  = this.getBufferStrategy();
        if (bs == null) {
            this.createBufferStrategy(3);   // recomandat 3 buffere, 3 layere
            return;
        }

        GraphSheet graphSheet = new GraphSheet(Game.bufferedImage);
        BufferedImage backgroundImg = graphSheet.grabImage(1, 21, WIDTH, HEIGHT);   // Background (row 20, col 1)

        Graphics g = bs.getDrawGraphics();
        g.drawImage(backgroundImg, 0, 0, null);
//        g.setColor(Color.LIGHT_GRAY);
//        g.fillRect(0,0, WIDTH, HEIGHT);

        if (paused) {
            BufferedImage pauseImg = graphSheet.grabImage(21, 23, 305, 306);
            g.drawImage(pauseImg, 160, 80, null); // henna
        }

        if (gameState == STATE.Game) {
            hud.render(g);  // pusa dupa lista de obiecte din handler ca sa fie deasupra ca layer, peste obiecte,
            handler.render(g);  // player si enemies sunt continute in faza asta
        }

        else if (gameState == STATE.Shop) {
            shop.render(g);  // in starea de shop nu mai face render de obiecte; ele sunt incarcate in handler, dar nu le vrem
             }

            else if (gameState == STATE.Menu || gameState == STATE.Help || gameState == STATE.End) {  // Daca e starea de Menu sau Help, imi apeleaza cls Menu, pt ca si Help e facut in Menu
                 menu.render(g);
                 handler.render(g);  // MenuAnimations sunt continute aici in faza asta
             }


        // Each Graphics object uses system resources, and these should be released.
        // Like needing to call close() on an InputStream that you have opened.
        g.dispose();
        bs.show();
    }

    // Metoda pentru a seta limite pt variabila var
    // Lucram cu float pt ca o vom folosi si pt coordonatele x, y care sunt float
    public static float clamp(float var, float min, float max) {
        if (var >= max)
            return var = max;   // nu poate depasi max sau min
        else if (var <= min)
            return var = min;
        else return var;
    }



    @Override
    public void run() {
        this.requestFocus();    // Sa nu mai fie nevoie de sa fac click pe fereastra ca sa putem avea ctrl asupra jocului cu tastele

        // Game loop - the heartbeat of the game, to make it actually work
        // Se copiaza efectiv codul asta, e popular
        long lastTime = System.nanoTime();
        double amountOfTicks = 60.0;
        double ns = 1000000000 / amountOfTicks;
        double delta = 0;
        long timer = System.currentTimeMillis();
        int frames = 0;
        while (running) {
            long now = System.nanoTime();
            delta += (now - lastTime) / ns;
            lastTime = now;
            while (delta >= 1) {
                tick();
                delta--;
            }
            if (running)
                render();
            frames++;

            if (System.currentTimeMillis() - timer > 1000) {
                timer += 1000;
                System.out.println("FPS " + frames);        // numarul de FPS
                frames = 0;
            }
        }
        stop();
    }




    public static void main(String[] args) {
        new Game();

    }


}
