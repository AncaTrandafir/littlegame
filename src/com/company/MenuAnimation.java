package com.company;

import java.awt.*;
import java.util.Random;

// MenuAnimation este obiect

public class MenuAnimation extends GameObject {

    private Handler handler;
    private Trail trail;
    private Random random;
    private Color randomColor;

    public MenuAnimation(int x, int y, ID id, Handler handler) {
        super(x, y, id);
        this.handler = handler;

        velX = 5;
        velY = 2;

        random = new Random();
        randomColor = new Color(random.nextInt(255), random.nextInt(255), random.nextInt(255));    // create random color

    }

    @Override
    public void tick() {
        x += velX;
        y += velY;

        // Daca este negativ(urca), negativ * negativ = pozitiv si deci se intoarce, coboara pe aceeasi traiectorie, daca sa zicem x = 0
        // Game.HEIGHT sa nu iasa din perimetrul ferestrei pe inaltime; Daca depaseste inaltimea ferestrei, se intoarce
        if (y <= 0 || y >= Game.HEIGHT - 32) velY *= -1;
        if (x <= 0 || x >= Game.WIDTH - 16) velX *= -1;

        handler.addObject(new Trail((int)x, (int)y, ID.Trail, randomColor, 4, 4, 0.01f, handler));
    }

    @Override
    public void render(Graphics g) {
        g.setColor(randomColor);
        g.fillRect((int)x, (int)y, 4, 4 );      // ultimele 2 valori dau dimensiunea: width si height
    }

    @Override
    public Rectangle getBounds() {              // nu avem intersectie
        return null;
    }



}
