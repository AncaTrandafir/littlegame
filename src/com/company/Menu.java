package com.company;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.util.Random;

import static com.company.Game.HEIGHT;
import static com.company.Game.WIDTH;

public class Menu extends MouseAdapter {

    private Game game;
    private Handler handler;
    private Random r;
    private HUD hud;
    private GraphSheet graphSheet;         // sheet -> unde este imaginea cu playerul nostru
    private BufferedImage menuImg, gameOverImg, playAgainImg, henna1, helpImg, henna3, backgroundEnd, audioImg;


    public Menu(Game game, Handler handler, HUD hud){         // in constructor importam game, handler ca sa importam Menu Animation si HUD ca sa importam hud.getScore pt a afisa scorul la GameOver
        this.game = game;
        this.handler = handler;
        this.hud = hud;

        graphSheet = new GraphSheet(Game.bufferedImage);   // instantiem un GraphSheet cu un BufferedImaged incarcat in Game cu loader();

        menuImg = graphSheet.grabImage(21, 6, 354, 249);  //  -> functia mea de a selecta subimagine pe baza: row 21, col 6, 354x249
        gameOverImg = graphSheet.grabImage(21, 33, 182, 110);
        playAgainImg = graphSheet.grabImage(11, 1, 130, 64);
        audioImg = graphSheet.grabImage(11, 4, 32, 32);

        henna1 = graphSheet.grabImage(21, 1, 130, 130);
        helpImg = graphSheet.grabImage(21, 14, 360, 285);
        henna3 = graphSheet.grabImage(29, 1, 104, 92);

        backgroundEnd = graphSheet.grabImage(1, 6, WIDTH, HEIGHT);


    }

    public void mousePressed(MouseEvent e){
        int mouseX = e.getX();          // coordonatele punctului de click
        int mouseY = e.getY();

        // Play button -> verific ca sunt in starea de meniu, sa nu inurc coordonatele cu alte stari, exemplu Shop

        if (game.gameState == Game.STATE.Menu) {


            if (mouseOver(mouseX, mouseY, 150, 90, 354, 83)) {   // dimensiunile dreptunghiului/butonului de meniu
                System.out.println("apasat butonul de play :)");

                // intai golim obiectele tip MenuAnimation care ruleaza in starea de meniu si sunt incarcate in lista de obiecte din handler
                for (int i = 0; i < handler.objectList.size(); i++)
                    handler.clearMenuAnimation();  // nu le sterge instantaneu, mai raman cateva secunde si cand incarca inamicii. de ce??

                game.gameState = Game.STATE.Game;  // gameState devine Game si cream obiectele; le-am mutat aici din constructorul Game; vezi comentat
                handler.addObject(new Player(WIDTH / 2 - 32, HEIGHT / 2 - 32, ID.Player, handler));
                r = new Random();
                handler.addObject(new BasicEnemy(r.nextInt(WIDTH), r.nextInt(HEIGHT), ID.BasicEnemy, handler));

                AudioPlayer.getSound("menuSound").play();  // incarc sunetul de click

            }
        }


        // Mute the volume
        if (game.gameState == Game.STATE.Menu) {
            if (mouseOver(mouseX, mouseY, WIDTH - 50, HEIGHT -75, 32, 32)) {   // dimensiunile dreptunghiului/butonului de meniu
                System.out.println("apasat butonul de volum");
                System.out.println(AudioPlayer.playing);
                if (AudioPlayer.playing == true) {
                    AudioPlayer.getMusic("music").pause();
                    AudioPlayer.playing = false; // pun pauza si setez pe false, nu mai canta
                } else AudioPlayer.getMusic("music").resume();
            }
        }



        // Quit button
        if (game.gameState == Game.STATE.Menu)
          if (mouseOver(mouseX, mouseY, 150, 250, 354, 83)) {   // dimensiunile dreptunghiului/butonului de meniu
                System.exit(1);
        }

        // Help button
        if (game.gameState == Game.STATE.Menu)
            if (mouseOver(mouseX, mouseY, 150, 170, 354, 83)) {   // dimensiunile dreptunghiului/butonului de help
                game.gameState = Game.STATE.Help;
                System.out.println("Apasat butonul Help");
                AudioPlayer.getSound("menuSound").play();
        }


        // Back button din Help, care are aceleasi coordonate cu Exit din STATE.Menu
        // Intai verific ca sunt in Help si apoi ca ma aflu in perimetrul respectiv
        if (game.gameState == Game.STATE.Help)
            if (mouseOver(mouseX, mouseY, 260, 320, 128, 64)) {
                System.out.println("apasat butonul de back :)");
                game.gameState = Game.STATE.Menu;  // back la starea de Menu

                AudioPlayer.getSound("menuSound").play();

                return;
        }


        // Play again button din starea de End
        if (game.gameState == Game.STATE.End)
             if (mouseOver(mouseX, mouseY, 260, 320, 128, 64)) {   // dimensiunile dreptunghiului/butonului de Play Again
                    game.gameState = Game.STATE.Game;
                     HUD.HEALTH = 100;       // reincepem jocul la parametri initiali de health, score, level
                     hud.setScore(0);
                     hud.setLevel(1);
                     for (int i=0; i< handler.objectList.size(); i++) {
                         handler.clearMenuAnimation();      // golim de animatiile din End state
                         handler.clearEnemies();  // nu ar trebui sa mai fie. dar cateodata ramane un enemy. Bug?
                     }
                     handler.addObject(new Player(WIDTH / 2 - 32, HEIGHT / 2 - 32, ID.Player, handler));      // reconstruim player si enemies
                     r = new Random();
                     handler.addObject(new BasicEnemy(r.nextInt(WIDTH), r.nextInt(HEIGHT), ID.BasicEnemy, handler));

                     AudioPlayer.getSound("menuSound").play();
             }

    }


    public void mouseReleased (MouseEvent e) {

    }


    public boolean mouseOver(int mouseX, int mouseY, int x, int y, int width, int height) {  // perimetrul butonului de apasat, sa putem apasa oriunde in interior
        if (mouseX > x && mouseX < x + width) {
            if (mouseY > y && mouseY < y + height)
                return true;   // este in spatiul delimitat de cele 4 puncte
            else return false;
        } else return false;



    }

    public void tick() {

    }

    public void render(Graphics g) {
        Font font = new Font("MV Boli", Font.PLAIN, 20);
        Font bigFont = new Font("MV Boli", Font.BOLD, 36);
        Font biggerFont = new Font("MV Boli", Font.BOLD, 40);
        g.setColor(Color.BLACK);

        if (game.gameState == Game.STATE.Menu) {        // creez meniul

            g.setFont(new Font("Forte", Font.PLAIN, 52));
            g.drawString("Henna Game", 185, 40);

//            g.setColor(Color.WHITE);
//            g.drawRect(210, 100, 200, 64);
//            g.setFont(smallerFont);
//            g.setColor(Color.WHITE);
//            g.drawString("Play", 290, 140);
//
//            g.setColor(Color.WHITE);
//            g.drawRect(210, 200, 200, 64);
//            g.setFont(smallerFont);
//            g.setColor(Color.WHITE);
//            g.drawString("Help", 290, 240);
//
//            g.setColor(Color.WHITE);
//            g.drawRect(210, 300, 200, 64);
//            g.setFont(smallerFont);
//            g.setColor(Color.WHITE);
//            g.drawString("Quit", 290, 340);

            g.drawImage(menuImg, 150, 90, null);  // desenam meniul

            g.setFont(bigFont);

            g.drawString("Play game", Game.WIDTH / 2 - 80 , 130);
            g.drawString("Help / About", Game.WIDTH / 2 - 118, 228);
            g.drawString("Quit", Game.WIDTH / 2 - 45, 310);

            g.drawImage(henna1, 5, 305, null);  // desenam henna

            g.drawImage(audioImg, WIDTH - 50, HEIGHT - 75, null);      // audio volume
        }

        if (game.gameState == Game.STATE.Help) {            // cream starea de HELP

            g.drawImage(helpImg,131, 20, null);      // instructiuni joc
            g.drawImage(playAgainImg, 260, 320, null);
            g.setFont(font);
            g.drawString("Back", 303, 351);

        }

        if (game.gameState == Game.STATE.End) {            // cream starea de End

            g.drawImage(backgroundEnd, 0, 0, null); // background
            g.drawImage(gameOverImg, 225, 40, null);

            g.setFont(font);
            g.drawString("You lost with a score of " + hud.getScore() + ".", 200, 225);
            g.drawString("You made it to level  " + hud.getLevel() + ".", 200, 265);

            g.setFont(font);
            g.drawImage(playAgainImg, 260, 320, null);
            g.drawString("Play again", 275, 356);
            g.drawImage(henna3, 4, 200, null);

        }

    }
}




    // !!!!!NOTE TO SELF:   Why Mouse Adapter and not Mouse Listener

//MouseAdapter:
//
// An abstract adapter class for receiving mouse events. The methods in this class are empty. This class exists as convenience for creating listener objects.
// Extend this class to create a MouseEvent (including drag and motion events) or/and MouseWheelEvent listener and override the methods for the events of interest
//
// In absence of MouseAdapter, if you implement MouseListener, you have to provide implementation to ALL of these interface methods.
//
    //mouseClicked(MouseEvent e)
    //mouseDragged(MouseEvent e)
    //mouseEntered(MouseEvent e)
    //mouseExited(MouseEvent e)
    //mouseMoved(MouseEvent e)
    //mousePressed(MouseEvent e)
    //mouseReleased(MouseEvent e)
    //mouseWheelMoved(MouseWheelEvent e)
// when would it be wise to use the one and when the other ?
//
// If you want to implement all above 8 methods, implement MouseListener.
// If you want to provide implementation for only some of these 8 methods, use MouseAdapter and override only those methods of interest for you.
//
// e.g. If you are interested only in implementing one event ( or few events) like mouseClicked(MouseEvent e) event, best to use MouseAdapter.
// If you implement MouseListener interface in this case, you have to provide blank implementation for other methods, which you are not going to implement.


