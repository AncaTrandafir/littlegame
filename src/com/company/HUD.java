package com.company;

import java.awt.*;

// Heads Up Display - area where players can see their character's vital statistics
// such as current health, attributes, armor level, ammunition count, and more.
public class HUD {

    public static float HEALTH = 100;   // type float pt ca vom folosi clamp care returneaza float

    private float greenValue = 255; // verde in RGB, daca R=0, G=255, B=0

    private int score = 0;
    private int level = 1;

    public int bounds = 0;


    public void tick() {
//        HEALTH--;       // descreste bara de viata    -> o folosesc direct cls Player, metoda collision()

        HEALTH = Game.clamp(HEALTH, 0, 100 + (bounds / 2));   // bounds impartit la 2 pt ca health e inmultit cu 2; vezi si Shop -> upgrade life


        greenValue = Game.clamp(greenValue, 0, 255); // o culoare e reprezentata de la 0 la 255
        greenValue = HEALTH * 2 - bounds * 2;        // health descreste, green creste si in combinatie cu celelalte culori da spre rosu

        score++; // se incrementeaza (pe baza faptului ca supravietuiesti, sa zicem; mai tarziu putem adauga si coins sa creasca scorul)

    }


    // !! Variabila bounds se introduce pt atunci cand playerul isi cumpara viata si se prelungeste dreptunghiul de viata

    public void render(Graphics g){         // draw the Health Bar
        g.setColor(Color.LIGHT_GRAY);
        g.fillRect(15, 15, 200 + bounds, 32);                // set the background HealthBar
        g.setColor(new Color(216, (int)greenValue, 78));         // imi creez singura culoarea, cu new Color si cei 3 parametri RGB; green nu e constanta, descreste (in tutorial are R75, B0)
        g.fillRect(15, 15, (int)HEALTH * 2, 32);         // set the bar for Health
        g.setColor(Color.WHITE);
        g.drawRect(15, 15, 200 + bounds, 32);         // set the outline

        g.setFont(new MyFont("Lucida Console", MyFont.BOLD, 16));
        g.setColor(Color.BLACK);
        g.drawString("Score " + score, 15, 67); // -> generates text; parametres: string, x and y coordinates
        g.drawString("Level  " + level, 15, 82);


    }


    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }
}
