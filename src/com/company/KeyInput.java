package com.company;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

// KeyAdapter is an abstract (adapter) class for receiving keyboard events.
// All methods are empty.
// This class is convenience class for creating listener objects.

public class KeyInput extends KeyAdapter {
    private Handler handler;    // lista de obiecte pe care o traversam
    private Game game;

    // sa nu se mai blocheze tastele cand ne miscam f repede
    // cream un vector de boolean pt a memora daca este apasata tasta
    private boolean[] keyDown = new boolean[4];

    public KeyInput(Handler handler, Game game) {
        this.handler = handler;
        this.game = game;           // in Game avem atributul Paused pe care il vom accesa pt a pune pauza

        keyDown[0] = false;     // initial WASD nu sunt apasate
        keyDown[1] = false;
        keyDown[2] = false;
        keyDown[3] = false;

    }


    /**
     * Notification from AWT that a key has been pressed. Note that
     * a key being pressed is equal to being pushed down but *NOT*
     * released.
     *
     * @param ev The details of the key that was pressed
     */
    public void keyPressed(KeyEvent ev) {
        int key = ev.getKeyCode();     // key-binding, shows number value corresponding of the pressed key: ASCII code ?
//        System.out.println(key);


        // SETAM TASTE DIFERITE PT PLAYERI DIFERITI
//        for (int i = 0; i < handler.objectList.size(); i++) {    // traversam lista
//            GameObject tempObj = handler.objectList.get(i);
//
//            if (tempObj.getId() == ID.Player)     // daca id-ul obiectului din handler este id de player1 sau player2
//                if (key == KeyEvent.VK_W) {        // daca keyEvent-ul are value key de W
//                    tempObj.setY(tempObj.getY() - 1); // decrementeaza Y cu o pozitie, adica urca un pas
//                }
//
//            if (tempObj.getId() == ID.Player2) // key event pt Player 2 - alta tasta
//                if (key == KeyEvent.VK_UP)         // daca keyEvent-ul are value key UP
//                    tempObj.setY(tempObj.getY() - 1);
//        }


        for (int i = 0; i < handler.objectList.size(); i++) {
            GameObject tempObj = handler.objectList.get(i);

            if (tempObj.getId() == ID.Player) {   // accesez variabila speed din Handler, pe care o folosesc cand cresc viteaza la shop; am grija sa pastrez semnele +, - pt directie

                if (key == KeyEvent.VK_W) {
                    tempObj.setVelY(-(handler.speed));  // se misca mai cursiv cu velocity decat cu X sau Y
                    keyDown[0] = true;
                }

                if (key == KeyEvent.VK_S) {
                    tempObj.setVelY(handler.speed);
                    keyDown[1] = true;
                }

                if (key == KeyEvent.VK_D) {
                    tempObj.setVelX(handler.speed);
                    keyDown[2] = true;
                }

                if (key == KeyEvent.VK_A) {
                    tempObj.setVelX(-(handler.speed));
                    keyDown[3] = true;

                }
                // handler.speed este 5 in faza initiala, pana sa incrementeze viteza la shop
            }
        }


        if (key == KeyEvent.VK_ESCAPE) System.exit(1);      // inchidem fereastra si cu ESC, pe langa click X


        if (key == KeyEvent.VK_P) {                 // pause the game cu tasta P
            if (Game.paused) Game.paused = false;
                else Game.paused = true;
        }


        if (key == KeyEvent.VK_SPACE) {
            if (game.gameState == Game.STATE.Game) {  // doar din starea de joc pot intra in Shop
                game.gameState = Game.STATE.Shop;
             } else if (game.gameState == Game.STATE.Shop) {
                 game.gameState = Game.STATE.Game;      // apas iar P cand sunt deja in Shop si ma intorc la Game
             }
        }           // in celelalte stari P nu face nimic


    }

    public void keyReleased(KeyEvent ev) {
        int key = ev.getKeyCode();

        for (int i = 0; i < handler.objectList.size(); i++) {
            GameObject tempObj = handler.objectList.get(i);

            if (tempObj.getId() == ID.Player) {
                if (key == KeyEvent.VK_W)
                  //  tempObj.setVelY(0);  // cand eliberam tasta, se opreste; altfel se tot duce :)        setam cu 0 velocitatea, sau setam variabila keyDown = false
                    keyDown[0] = false;

                if (key == KeyEvent.VK_S)
                    //  tempObj.setVelY(0);
                    keyDown[1] = false;

                if (key == KeyEvent.VK_D)
                    //  tempObj.setVelX(0);
                    keyDown[2] = false;

                if (key == KeyEvent.VK_A)
                    //   tempObj.setVelX(0);
                    keyDown[3] = false;


                // cod in plus ca sa se miste player-ul fara intreruperi (liniile de mai sus comprimate)

                // vertical movement
                if (!keyDown[0] && !keyDown[1]) tempObj.setVelY(0);
                // horizontal movement
                if (!keyDown[2] && !keyDown[3]) tempObj.setVelX(0);
            }
        }


    }

}
