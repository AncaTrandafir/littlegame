package com.company;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;

public class Shop extends MouseAdapter {   // reactioneaza la click-ul Mouse-ului

    private Game game;
    private Handler handler;
    private HUD hud;

    private GraphSheet graphSheet;
    private BufferedImage healthImg, lifeImg, speedImg, buttonsImg, scoreImg;

    private int costSpeed = 1000;
    private int costHealth = 2000;
    private int costLife = 4000;


    public Shop (Game game, Handler handler, HUD hud){       // upgrade health, upgrade speed, refill health
        this.handler = handler;
        this.hud  =  hud; // incarc hud pt scor
        this.game = game; // incarc game pt STATE


        graphSheet = new GraphSheet(Game.bufferedImage);   // instantiem un GraphSheet cu un BufferedImaged incarcat in Game cu loader();
        speedImg = graphSheet.grabImage(4, 3, 64, 64); // os
        lifeImg = graphSheet.grabImage(3, 1, 64, 64); // creste bara
        healthImg = graphSheet.grabImage(1, 1, 64, 64); // creste health

        buttonsImg = graphSheet.grabImage(21, 6, 354, 249);
        scoreImg = graphSheet.grabImage(6, 4, 35, 40);

    }



// !!!!! Conteaza foarte mult sa precizez si starea cand dau click in perimetru, altfel ia coordonatele din celelalte stari (Game, Menu), face bulibaseala

    public void mousePressed(MouseEvent e){
        int mouseX = e.getX();      // coordonatele punctului de click
        int mouseY = e.getY();


        if (game.gameState == Game.STATE.Shop) {            // Verific ca sunt in starea de Shop

            // Box Health
            if (mouseX >= 150 && mouseX <= 504) {    // sunt dimensiunile randului din tabela de butoane, calculate
                if (mouseY >= 90 && mouseY <= 165) {
                    System.out.println("Box Speed");
                    if (hud.getScore() >= costSpeed) {
                        hud.setScore(hud.getScore() - costSpeed);
                        costSpeed += 500; // daca vreau sa mai cumpar o data se dubleaza;
                        handler.speed++;      // incrementez viteza, care e setata in handler
                    }
                }
            }

            // Box Health
            if (mouseX >= 150 && mouseX <= 504) {
                if (mouseY >= 170 && mouseY <= 245) {
                    System.out.println("Box Health");
                    if (hud.getScore() >=  costHealth) {
                        hud.setScore(hud.getScore() - costHealth);
                        costHealth += 1000;
                        hud.HEALTH = (100 + (hud.bounds / 2)); // full health
                    }
                }
            }

            // Box Life
            if (mouseX >= 150 && mouseX <= 504) {
                if (mouseY >= 250 && mouseY <= 330) {
                    System.out.println("Box Life");
                    if (hud.getScore() >= costLife) {
                        hud.setScore(hud.getScore() - costLife);
                        costLife += 2000;
                        // health este clamp-uita.. si atunci introducem variabila bounds in cls Hud
                        hud.bounds += 20;  // crestem dreptunghiul de viata cu 20
                        // verdele porneste de la val 100 si, avand in vedere ca health este inmultit cu 2, bounds treb impartit la 2; vezi hud.bounds din Hud (sunt calcule matematice..)
                        hud.HEALTH = (100 + (hud.bounds / 2));
                    }
                }
            }

        }

    }





    public void render(Graphics g) {

        g.drawImage(buttonsImg, 150, 90, null );

        g.drawImage(speedImg, Game.WIDTH / 2 - 150, 95, null);
        g.drawImage(healthImg, Game.WIDTH / 2 - 150, 185, null);
        g.drawImage(lifeImg, Game.WIDTH / 2 - 150, 275, null);

//        MyFont myFont = new MyFont("MyFont", 1, 12);
//        g.setFont(myFont);
//        g.drawString("Shop", Game.WIDTH / 2 - 50, 50);

        Font bigFont = new Font("MV Boli", Font.BOLD, 36);
        Font smallFont = new Font("MV Boli", Font.PLAIN, 20);

        g.setColor(Color.BLACK);
        g.setFont(bigFont);
        g.drawString("Shop", Game.WIDTH / 2 - 50, 60);

        g.setFont(smallFont);

        g.drawString("Upgrade speed", Game.WIDTH / 2 - 30 , 120);
        g.drawString("Cost: " + costSpeed, Game.WIDTH / 2 - 30 , 140);
        g.drawString("Refill health", Game.WIDTH / 2 - 30, 210);
        g.drawString("Cost: " + costHealth, Game.WIDTH / 2 - 30, 230);
        g.drawString("Upgrade health", Game.WIDTH / 2 - 30, 300);
        g.drawString("Cost: " + costLife, Game.WIDTH / 2 - 30, 320);

        g.drawImage(scoreImg, Game.WIDTH / 2 - 80, 360, null);
        int score = hud.getScore();
        g.drawString("Your score: " + score, Game.WIDTH / 2 - 30, 385);
    }


}
