package com.company;

public enum ID {        // creates an enumeration and now we can id player or enemy by its id
    Player(),
  //  Player2(),
    BasicEnemy(),
    Trail(),
    FastEnemy(),
    SmartEnemy(),
    BossEnemy(),
    BulletBossEnemy(),
    MenuAnimation();
}
