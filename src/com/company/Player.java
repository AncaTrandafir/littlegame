package com.company;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Random;

public class Player extends GameObject {

    Random r = new Random();

    Handler handler;
    GraphSheet graphSheet;
    private BufferedImage playerImage;

    public Player(int x, int y, ID id, Handler handler) {           // incarc imagimea mea in constrcutor
        super(x, y, id);
        this.handler = handler;

        graphSheet = new GraphSheet(Game.bufferedImage);   // instantiem un GraphSheet cu un BufferedImaged incarcat in Game cu loader();
//        playerImage = graphSheet.grabImage(5, 2, 32, 32);  //  -> functia mea de a selecta subimagine pe baza: row 1, col 1, 32x32

        playerImage = graphSheet.grabImage(8, 4, 64, 40);
    }

    @Override
    public void tick() {
        x += velX;
        y += velY;

        x = Game.clamp((int)x, 0, Game.WIDTH - 76);      // cu met clamp setam limite pt x, intre 0 si inaltime/latime fereastra
        y = Game.clamp((int)y, 0, Game.HEIGHT - 79);

        collision();

    //    handler.addObject(new Trail((int)x, (int)y, ID.Trail, Color.WHITE, 32, 32, 0.1f, handler));
    }

    @Override
    public void render(Graphics g) {

//          Daca as avea 2 jucatori
//        if (id == ID.Player) g.setColor(Color.MAGENTA);
//        else if (id == ID.Player2) g.setColor(Color.BLUE);

//        g.setColor(Color.YELLOW);                      // Player dreptunghi
//        g.fillRect((int)x, (int)y, 32, 32);

        // Playerul meu customizat
        g.drawImage(playerImage, (int)x, (int)y, null);

    }


    @Override
    public Rectangle getBounds() {
        return new Rectangle((int)x, (int)y, 64, 40);     // sunt dimensiunile playerului meu;
                                                              // la fel setam si pt enemy cu care ne vom intersecta
    }

    // Ca sa putem intersecta doua obiecte avem nevoie de lista de obiecte, deci in constructorul lui Player punem si handler
    // Nu aveam nevoie inainte de handler in constructor Player.


    public void collision(){
        for (int i=0; i < handler.objectList.size(); i++) {
            GameObject tempObject = handler.objectList.get(i);

            if (tempObject.getId() == ID.BasicEnemy || tempObject.getId() == ID.FastEnemy || tempObject.getId() == ID.SmartEnemy || tempObject.getId() == ID.BossEnemy || tempObject.getId() == ID.BulletBossEnemy) {
                // collision code : daca dreptunghiul de bounds al Playerului(sunt in cls Player) intersecteaza dreptunghiul de bounds al Enemy
                if (getBounds().intersects(tempObject.getBounds())) {
                    // descreste viata
                    HUD.HEALTH --;
                }
            }
        }
    }


}
