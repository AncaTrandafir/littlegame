package com.company;

import java.awt.image.BufferedImage;

public class GraphSheet {

    // BufferedImage objects have an upper left corner coordinate of (0, 0)
    private BufferedImage img;

    public GraphSheet (BufferedImage img) {
        this.img = img;

    }


    public BufferedImage grabImage(int col, int row, int width, int height){
        BufferedImage image = img.getSubimage((col * 32) - 32, (row * 32) - 32, width, height);   // primeste coordonate x,y ale top corner si latime si inaltime
                // transform (x,y) in cod pt a selecta rand si coloana
    return image;

    }


}
