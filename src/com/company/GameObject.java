package com.company;

import java.awt.*;

public abstract class GameObject {      // all the game objects: players, enemies, coins etc; like general entity
    protected float x, y;
    protected ID id;
    protected float velX, velY;   // for speed

    public GameObject(float x, float y, ID id) {          //  constructor
        this.x = x;
        this.y = y;
        this.id = id;
    }

    // https://beginnersbook.com/2014/01/abstract-method-with-examples-in-java/

    public abstract void tick();                    // it's abstract so i'm gonna need it in all my classes, they will be overridden.

    public abstract void render(Graphics g);        // tick si render nu sunt ascunse ca get si set de mai jos care vor fi chemate in cls mele

    public abstract Rectangle getBounds();          // folosim cls Rectangle pt a trata coliziunile; cand se intersecteaza 2 dreptunghiuri, inseamna coliziune in jocul nostru
                                                    // -> Rectangle has an intersect method that returns true or false

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public ID getId() {
        return id;
    }

    public void setId(ID id) {
        this.id = id;
    }

    public float getVelX() {
        return velX;
    }

    public void setVelX(float velX) {
        this.velX = velX;
    }

    public float getVelY() {
        return velY;
    }

    public void setVelY(float velY) {
        this.velY = velY;
    }
}
