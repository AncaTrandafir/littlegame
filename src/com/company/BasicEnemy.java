package com.company;

import java.awt.*;
import java.awt.image.BufferedImage;

public class BasicEnemy extends GameObject {

    private Handler handler;
    private Trail trail;
    private GraphSheet graphSheet;
    private BufferedImage basicEnemyImage;

    public BasicEnemy(int x, int y, ID id, Handler handler) {
        super(x, y, id);
        this.handler = handler;

        velX = 2;
        velY = 2;

        graphSheet = new GraphSheet(Game.bufferedImage);   // instantiem un GraphSheet cu un BufferedImaged incarcat in Game cu loader();
        basicEnemyImage = graphSheet.grabImage(5, 1, 32, 32);  //  -> functia mea de a selecta subimagine pe baza: row 1, col 3, 16x16

    }

    @Override
    public void tick() {
        x += velX;
        y += velY;

        // Daca este negativ(urca), negativ * negativ = pozitiv si deci se intoarce, coboara pe aceeasi traiectorie, daca sa zicem x = 0
        // Game.HEIGHT sa nu iasa din perimetrul ferestrei pe inaltime; Daca depaseste inaltimea ferestrei, se intoarce
        if (y <= 0 || y >= Game.HEIGHT - 64) velY *= -1;
        if (x <= 0 || x >= Game.WIDTH - 40) velX *= -1;

        // La enemy, adaugam obiect tip trail
   //     handler.addObject(new Trail((int)x, (int)y, ID.Trail, Color.GREEN, 16, 16, 0.08f, handler));  // 0.08 pt viata
    }

    @Override
    public void render(Graphics g) {
//        g.setColor(Color.GREEN);
//        g.fillRect((int)x, (int)y, 16, 16 );      // ultimele 2 valori dau dimensiunea: width si height
            g.drawImage(basicEnemyImage, (int)x, (int)y, null);
    }

    @Override
    public Rectangle getBounds() {              // returneaza un dreptunghi de dimensiunile astea
        return new Rectangle((int)x, (int)y, 16, 16);
    }



}
