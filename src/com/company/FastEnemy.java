package com.company;

import java.awt.*;
import java.awt.image.BufferedImage;

public class FastEnemy extends GameObject {

    private Handler handler;
    private Trail trail;
    GraphSheet graphSheet;
    BufferedImage fastEnemyImage;

    public FastEnemy(int x, int y, ID id, Handler handler) {
        super(x, y, id);
        this.handler = handler;

        velX = 5;
        velY = 5;

        graphSheet = new GraphSheet(Game.bufferedImage);   // instantiem un GraphSheet cu un BufferedImaged incarcat in Game cu loader();
        fastEnemyImage = graphSheet.grabImage(5, 5, 16, 16);

    }

    @Override
    public void tick() {
        x += velX;
        y += velY;

        // Daca este negativ(urca), negativ * negativ = pozitiv si deci se intoarce, coboara pe aceeasi traiectorie, daca sa zicem x = 0
        // Game.HEIGHT sa nu iasa din perimetrul ferestrei pe inaltime; Daca depaseste inaltimea ferestrei, se intoarce
        if (y <= 0 || y >= Game.HEIGHT - 32) velY *= -1;
        if (x <= 0 || x >= Game.WIDTH - 16) velX *= -1;

        // La enemy, adaugam obiect tip trail
     //   handler.addObject(new Trail((int)x, (int)y, ID.Trail, Color.ORANGE, 10, 10, 0.06f, handler));  // 0.1 pt viata
    }

    @Override
    public void render(Graphics g) {
//        g.setColor(Color.ORANGE);
//        g.fillRect((int)x, (int)y, 10, 10 );      // ultimele 2 valori dau dimensiunea: width si height
            g.drawImage(fastEnemyImage, (int)x, (int)y, null);
    }

    @Override
    public Rectangle getBounds() {              // returneaza un dreptunghi de dimensiunile astea
        return new Rectangle((int)x, (int)y, 10, 10);
    }



}
