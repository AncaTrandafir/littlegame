package com.company;

import java.awt.*;
import java.io.IOException;

public class MyFont extends Font {

    public MyFont(String name, int style, int size) {
        super(name, style, size);
    }

    public void loadFont(){
        GraphicsEnvironment ge = null;
        try{
            ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
            ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, this.getClass().getResourceAsStream("res/font1.otf")));
        } catch(FontFormatException e){} catch (IOException e){}
    }

}
